package secondweekpractice;

/*
Даны три числа a, b, c.
Найти сумму двух чисел больших из них.
Входные данные
21 0 8
Выходные данные
29
 */

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();


        // Можно так же
        //System.out.println("Сумма =" + (Math.max(Math.max(a + b,b + c), a + c)));

        if(a >= b && b >= c) {
            System.out.println(a + b);
        }
        else if(a >= c && c >= b) {
            System.out.println(a + c);
        }
        else if(c >= a && b >=  a) {
            System.out.println(c + b);
        }
    }
}
