package secondweekpractice;

import java.util.Scanner;
/*
/*
Дано число n. Нужно проверить четное ли оно.
 */

public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int i = scan.nextInt();
        String str;

//        /*if (i % 2 == 0) {
//            str = " Число чётное";
//        } else {
//            str = " Число нечётное";
//        }


        str = (i % 2 ==0) ? " Число чётное" : " Число нечётное";
        System.out.println(i + str);
    }
}
