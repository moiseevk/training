package secondweekpractice;

/*
Даны три целых числа a, b, c.
Проверить есть ли среди них прямо противоположные.
( 5 и -5 прямо противоположные числа).
0 и 0 не считать прямо противоположными.
Входные данные
-1 1 0
Выходные данные
true
Входные данные
-2 1 0
Выходные данные
false
 */


import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();



        // Можно ещё так
        //System.out.println((a+b==0&&a!=0 || b+c==0&&b!=0 || a+c ==0&&c!=0)?"true":"false");

        if (Math.abs(a) == Math.abs(b) || Math.abs(a) == Math.abs(c) || Math.abs(b) == Math.abs(c)){
            System.out.println("true");
        } else {
            System.out.println("false");
        }

    }
}
