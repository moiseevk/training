package secondweekpractice;

/*
   Дана строка и паттерн, заменить паттерн на паттерн, состоящий из заглавных символов
   Входные данные
   Hello
   o
   Выходные данные
   HellO

   Входные данные
   Hello world
   ld
   Выходные данные
   Hello worLD
    */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);

        String str = scan.nextLine();
        String pattern = scan.next();

        String upperPattern = pattern.toUpperCase();
        System.out.println(str.replace(pattern,upperPattern));


    }
}
