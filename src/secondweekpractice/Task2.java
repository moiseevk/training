package secondweekpractice;

/*
Дано число n.
Если оно четное и больше либо равно 0, то вывести “Четное больше или равно 0”.
Если четное и меньше 0, то вывести “Четное меньше 0”.
 */


import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int i = scan.nextInt();

        if(i % 2 == 0 && i >= 0 ) {
            System.out.println("Четное больше или равно 0");
        }
        else if(i % 2 ==0 && i < 0) {
            System.out.println("Четное меньше 0");
        }
    }
}
