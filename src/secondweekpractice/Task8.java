package secondweekpractice;

/*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
A
Выходные данные
a
 */

import java.util.Scanner;
import java.lang.Character;
public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String symbol = scan.next();
        char c = symbol.charAt(0);

        if(c >= 'a' && c <= 'z') {
            System.out.println((char) (c + ('A' - 'a')));
        }
        else {
            System.out.println((char) (c - ('A' - 'a')));
        }



//        char ch = scan.next().charAt(0);
//        if(Character.isLowerCase(ch)) {
//            System.out.println(Character.toUpperCase(ch));
//        }
//        if (Character.isUpperCase(ch)) {
//            System.out.println(Character.toLowerCase(ch));
//        }



    }
}
