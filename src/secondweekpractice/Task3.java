package secondweekpractice;

/*
Дано четырехзначное число. Проверить является ли оно палиндромом.
1881 -> true
5081 -> false
 */
// Вот тут подумать надо!

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();

        if(n > 999 && n < 10000) {
            int end = n % 10;
            int start = n / 1000;
            if(end != start) {
                System.out.println("Число не палиндром");
            }
            else {
                end = (n % 100) / 10;
                start = (n / 100) % 10;
                if(end != start) {
                    System.out.println("не палиндром");
                }
                else {
                    System.out.println("палиндром");
                }
            }


        }
        else {
            System.out.println("Ввели некорректное число(4 знака должно быть)");
        }

    }
}
