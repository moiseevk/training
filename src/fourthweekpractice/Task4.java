package fourthweekpractice;

import java.util.Scanner;

/*
Начальный вклад в банке равен 1000.
Каждый месяц размер вклада увеличивается на P процентов от имеющейся суммы (0 < P < 25).
Найти через какое количество времени размер клада будет больше 1100
и вывести найденное количество месяцев и итоговый размер вклада.
15 ->
1
1150.0
3 ->
4
1125.50881
 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int p = scan.nextInt();
        double amount = 1000;
        double limit = 1100;
        int month = 0;
        while(amount < limit) {
            amount += + amount * p / 100;
            month++;
        }
        System.out.println(month);
        System.out.println(amount);
    }
}
