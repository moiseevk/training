package fourthweekpractice;

import java.util.Scanner;

/*
Дана строка s
Вычислить количество символов в ней не считая пробелов
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);

        String s = scan.nextLine();
//
//        Так сделал я
//        int count = 0;
//        char pr = ' ';
//
//        for (int i = 0; i < s.length(); i++) {
//            char ch = s.charAt(i);
//            if(ch == pr) {
//                count++;
//            }
//        }
//        System.out.println(s.length() - count);

        // Решение преподавателя

        int result = 0;
        for (int i = 0; i < s.length(); i++){
            if (s.charAt(i) != ' ') {
                result++;
            }
        }
        System.out.println(result);
    }
}
