package fourthweekpractice;

import java.util.Scanner;

/*
На вход подаётся число n и последовательность целых чисел длины n.
Вывести два максимальных числа в этой последовательности без использования массивов.
5
1 3 5 4 5 -> 5 5
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);
        int n = scan.nextInt();

        int firstNumber =  scan.nextInt();
        int secondNumber = scan.nextInt();

        int firstMax = Math.max(firstNumber, secondNumber);
        int secondMax = Math.min(firstNumber,secondNumber);

        for(int i = 2; i < n;i++) {
            int k = scan.nextInt();
            if (k > firstMax) {
                secondMax = firstMax;
                firstMax = k;
            }
            else if (k > secondMax) {
                secondMax = k;
            }

        }
        System.out.println(firstMax + " " + secondMax);
    }
}
