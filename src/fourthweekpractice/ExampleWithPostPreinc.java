package fourthweekpractice;

public class ExampleWithPostPreinc {
    /*
    Примеры на пре-пост инкремент
     */
    public static void main(String[] args) {
        // post-inc
//        int i = 1;
//        int a = i++;
//
//        int temp = i;
//        i = i + 1;
//        a = temp;
//        System.out.println(i);
//        System.out.println(a);

        // pre-inc
        int i = 1;
        int a = ++i;
        System.out.println("i равно: " + i);
        System.out.println("a равно: " + a);

    }
}
