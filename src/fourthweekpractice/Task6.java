package fourthweekpractice;

import java.util.Scanner;

/*
Дано целое число n.
Найти A число Фибоначии с помощью цикла.
// 0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946
F0 = 0;
F1 = 1;
Fn = Fn-1 + FN-2;
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();

        int fib0 = 0;
        int fib1 = 1;

        if (n == 0) {
            System.out.println(fib0);
            return;
        }
        if (n == 1) {
            System.out.println(fib1);
            return;
        }
        int fibResult = 0;

        for(int i = 2; i <= n; i++) {
            fibResult = fib0 + fib1;
            fib0 = fib1;
            fib1 = fibResult;
        }
        System.out.println(fibResult);
    }
}
