package fourthweekpractice;
/*
Даны числа m < 13  и < 7
Вывести все степени ( от  0 до n включительно) числа m с помощью цикла.
3 6
->
1
3
9
27
81
243
729
 */
import java.util.Scanner;
public class Task3 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);

        int m =  scan.nextInt();
        int n =  scan.nextInt();

//        case 1 with Math.Pow
//        for(int i = 0; i <= n; i++) {
//            System.out.println((int)Math.pow(m,i));
//        }

//        case 2 without Pow
//        int res = 1;
//        System.out.println(res);
//        for (int i = 1; i<= n; i++) {
//            res *= m;
//            System.out.println(res);
//        }
//
//         case 3 with while

//        int res = 1;
//        int amount = 1;
//
//        while(amount <= n) {
//            res *= m;
//            System.out.println(res);
//            amount++;
//        }

        // case 4 with do while
        int res = 1;
        int i = 1;
        do {
            System.out.println(res);
            res *= m;
            i++;
        } while ( i <= n + 1);
    }
}
