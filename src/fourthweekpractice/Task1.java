package fourthweekpractice;

import java.util.Scanner;

/*
Дано число n < 13, n > 0.
Найти факториал числа n (n! = 1 * 2 * 3 * _ * (n - 1) * n)
7 -> 5040
 */
public class Task1 {
   public static void main(String[] args) {
       Scanner scan = new Scanner(System.in);
       int n = scan.nextInt();

       int fact = 1;
       for (int i = 2; i <= n; i++) {
           fact *= i;
           System.out.println("Промежуточный результат: " + fact);
       }
       System.out.println("Итоговый результат: " + fact);
   }
}
