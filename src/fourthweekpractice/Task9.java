package fourthweekpractice;

import java.util.Scanner;

/*
Дана последовательность из n целых чисел, которая может начинаться с отрицательного числа. Определить, какое количество
отрицательных чисел записано в начале последовательности
и прекратить выполнение программы при получении первого неотрицательного числа на вход.
Входные данные:
-1
-2
4
Выходные данные:
Rexult: 2
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);

        int n = scan.nextInt();
        int result = 0;
//        1 решение
//        do {
//            result++;
//            n = scan.nextInt();
//        } while(n < 0);
//        System.out.println(result);


//      2 решение
//        while(n < 0) {
//            result++;
//            n = scan.nextInt();
//        }
//        System.out.println(result);

//        3 решение
        for (int i = n; i < 0; i = scan.nextInt()){
            result++;
        }
        System.out.println(result);
    }
}
