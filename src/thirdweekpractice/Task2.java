package thirdweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);
        String cardNumber = scan.nextLine();
        String code = scan.nextLine();

        System.out.println("Card number is valid: " + cardNumber.matches("([0-9]{4} ){3}[0-9]{4}"));
        System.out.println("PIN is valid: " + code.matches("[0-9]{4}"));
    }
}
