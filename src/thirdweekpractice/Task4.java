package thirdweekpractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        String inputStr = new Scanner(System.in).nextLine();

        System.out.println("Initial value: " + inputStr);
        System.out.println("Updated value: " + inputStr.replaceAll("([a-z]+)([A-Z]+)",
                "$1_$2").toLowerCase());

    }
}
