package thirdweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);
        String name = scan.nextLine();
        String date = scan.nextLine();
        String phoneNumber = scan.nextLine();
        String email = scan.nextLine();

        System.out.println("Our name is valid: " + name.matches("[А-ЯA-Z][а-яa-z]{1,19}"));
        System.out.println("Our data is valid: " + date.matches("(\\d{2}.){2}\\d{4}"));
        System.out.println("Our phoneNumber is valid: " + phoneNumber.matches("\\+\\d{11}"));
        System.out.println("Our email is valid: " + email.matches("[A-Za-z0-9\\-\\_\\*\\.]+@[a-z0-9]+\\.(com|ru)"));

    }
}
