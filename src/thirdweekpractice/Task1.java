package thirdweekpractice;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        String inputColor = new Scanner(System.in).nextLine();
        System.out.println("Our input data: " + inputColor);

        //Первое регулярное выражение на Java
        System.out.println(inputColor.matches("#[a-fA-F0-9]{6}"));

    }
}
