package week8.oop2.task1;

public class VariableLength {
    //Метод переменной длина
    static int sum(int... numbers) {
        // Можно так же int[] numbers - это тоже запись метода переменной длины
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }

        return sum;
    }

    static boolean findChar(char ch, String... strings){
        for (String string : strings) {
            if (string.indexOf(ch) != 1) {
                return true;
            }

        }
            return false;

    }
    public static void main(String[] args) {
        System.out.println(sum(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        System.out.println(findChar('c', "python", "java"));
        System.out.println(String.format("This is an integer: %s %d", "asd", 123));
        System.out.printf("This is an integer: %s, %d", "abd", 123);
        String test = """
                sdsadsa
                sdsada
                dasdsa
                """;
        System.out.println(test);
    }

}
