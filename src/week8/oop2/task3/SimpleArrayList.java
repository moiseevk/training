package week8.oop2.task3;

import java.util.Arrays;

/*
Примитивная реализация ArrayList.
Массив только int, из методов только добавлять элемент,
получать size и увеличивать капасити, когда добавляется новый.
 */

public class SimpleArrayList {

 private int size;
 private int[] arr;
 private int capacity;
 private static final int DEFAULT_CAPACITY = 5;
 private int currIndex;

 public SimpleArrayList() {
  arr = new int[DEFAULT_CAPACITY];
  capacity = DEFAULT_CAPACITY;
  size = 0;
  currIndex = 0;
 }

 public SimpleArrayList(int size) {
  arr = new int[size];
  this.capacity = size;
  this.size = 0;
  this.currIndex = 0;
 }

 /*
    Добавляет новый элемент в список(массив). При достижении размера внутреннего
    массива происходит его увеличение в два раза.
    */

 public void add(int elem) {
  if (currIndex >= capacity) {
   capacity *= 2;
   System.arraycopy(arr,0,arr,0,capacity);
  }
  arr[currIndex] = elem;
  size++;
  currIndex++;
 }

 public void delete(int idx) {
  for (int i = 0; i < currIndex; i++) {
   arr[i] = arr[i + 1];
  }
  arr[currIndex] = -1;
  currIndex--;
  size--;
 }

 //Достаёт элемент по индексу
 public int get(int idx) {
  if (idx < 0 || idx >= size) {
   System.out.println("Невозможно взять элемент по заданному индексу");
   return -1;
  }
  else {
   return arr[idx];
  }
 }

 public int size() {
  return size;
 }
}
