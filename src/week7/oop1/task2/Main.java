package week7.oop1.task2;

public class Main {
    public static void main(String[] args) {
        Thermometer thermometer =  new Thermometer(-25, TemperatureUnit.CELSIUS);
        System.out.println("В цельсиях: " + thermometer.getTempCelcius());
        System.out.println("В фаренгейтах: " + thermometer.getTempFahrenheit());

    }
}
