package week7.oop1.task2;
/*
Реализовать класс “Термометр”.
Необходимо иметь возможность создавать инстанс(экземпляр) класса с текущей температурой и
получать значение в фаренгейте и в цельсии.
 */

public class Thermometer {



    private double tempCelcius;
    private double tempFahrenheit;

    public Thermometer(double currentTemperature, TemperatureUnit temperatureUnit) {
        if (temperatureUnit == TemperatureUnit.CELSIUS) {
            tempCelcius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
        }
        else if (temperatureUnit == TemperatureUnit.FAHRENHEIT) {
            tempFahrenheit = currentTemperature;
            tempCelcius = fromFahrenheitToCelsius(currentTemperature);
        }
        else {
            System.out.println("Температура не распознана. Единица измерения по умолчанию = цельсий");
            tempCelcius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
//            System.out.println("ERROR! ALARM!");
//            throw new UnsupportedOperationException("Температура не распознана");
        }
    }

    public double getTempCelcius() {
        return tempCelcius;
    }

    public double getTempFahrenheit() {
        return tempFahrenheit;
    }


    private double fromCelsiusToFahrenheit(double currentTemperature) {
        return currentTemperature * 1.8 + 32;
    }

    public double fromFahrenheitToCelsius(double currentTemperature) {
        return (currentTemperature - 32) / 1.8;
    }
}
