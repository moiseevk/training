package week7.oop1.task4;

public enum Direction {
    UP(0),
    RIGHT(1),
    BOTTOM(2),
    LEFT(3);

    public final int number;

    Direction(int number) {
        this.number = number;
    }

    public static Direction ofNumber(int number) {
        for (Direction direction : values()) {
            if (direction.number == number) {
                return direction;
            }

        }
        return null;
    }

}
