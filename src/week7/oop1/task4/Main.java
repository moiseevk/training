package week7.oop1.task4;

public class Main {
    public static void main(String[] args) {
        Robot robot = new Robot();

        robot.go();
        robot.go();
        robot.go();
        robot.go();
        robot.turnRight();
        robot.go();
        robot.go();
        robot.go();
        robot.turnRight();
        robot.turnLeft();
        robot.go();
        robot.go();
        robot.go();
        robot.turnRight();
        robot.go();
        robot.printCoordinates();
    }
}
