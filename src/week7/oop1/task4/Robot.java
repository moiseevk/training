package week7.oop1.task4;
/*
Робот.
Команды повернуть влево, повернуть вправо, идти на 1 шаг.
Несколько конструкторов, хранение координат, вывод потом координат на экран.
 */

public class Robot {

    private int x;
    private int y;
    // private int direction; //0 -- up, 1 -- right, 2 -- bottom, 3 -- left
    private Direction direction;

    public Robot() {
        this.x = 0;
        this.y = 0;
        this.direction = Direction.UP;
    }

    public Robot(int x, int y) {
        this.x = x;
        this.y = y;
        this.direction = Direction.UP;
    }

    public Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public void go() {
        System.out.println("Initial Y: " + y);
        System.out.println("Initial X: " + x);
        System.out.println("Initial direction: " + direction);
        System.out.println("-----------------------------");
        System.out.println("!!!! GO GO GO !!!!");
        System.out.println("-----------------------------");
        switch (direction) {
            //top
            //case 0 -> y++;
            case UP -> {
                y++;
            }

            //right
            //case 1 -> x++;
            case RIGHT -> {
                x++;
            }

            //bottom
            //case 2 -> y--;
            case BOTTOM -> {
                y--;
            }

            //left
            //case 3 -> x--;
            case LEFT -> {
                x--;
            }
        }
//        System.out.println("!!!! STOP WALKING !!!!");
//        System.out.println("-----------------------------");
//        System.out.println("After walk Y: " + y);
//        System.out.println("After walk X: " + x);
//        System.out.println("After walk direction: " + direction);
//        System.out.println("-----------------------------");
        printCoordinates();
    }

    //0 -- up, 1 -- right, 2 -- bottom, 3 -- left
    public void turnLeft() {
        System.out.println("!!!! TURNING LEFT !!!!");
        System.out.println("-----------------------------");
        //this.direction = (direction - 1) % 4;
        this.direction = Direction.ofNumber((this.direction.number + 3) % 4);
    }

    public void turnRight() {
        System.out.println("!!!! TURNING RIGHT !!!!");
        System.out.println("-----------------------------");
        //this.direction = (direction + 1) % 4;
        this.direction = Direction.ofNumber((this.direction.number + 1) % 4);
    }

    public void printCoordinates() {
        System.out.println("(x,y) == " + x + ", " + y);
    }



}
