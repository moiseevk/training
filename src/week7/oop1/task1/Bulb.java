package week7.oop1.task1;
/*
Реализовать класс “Лампа”. Методы:
включить лампу
выключить лампу
получить текущее состояние
 */

public class Bulb {
   private boolean toggle;

   public Bulb() {
      this.toggle = false;
   }

   public Bulb(boolean toogle) {
      this.toggle = toogle;
   }

   public void turnOn() {
      this.toggle = true;
   }

   public void turnOff() {
      this.toggle = false;
   }

   public boolean getToggle() {
      return this.toggle;
   }


}
