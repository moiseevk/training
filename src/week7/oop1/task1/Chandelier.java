package week7.oop1.task1;
/*
Включать / выключать люстру и показывать ее состояние
В люстре есть много ламп
 */
public class Chandelier {
    private Bulb[] chandelier;

    // Конструктор люстры - передаём количество лампочек и создаём объекты ламп в массив.
    public Chandelier(int countOfBulbs) {
        chandelier = new Bulb[countOfBulbs];
        for (int i = 0; i < countOfBulbs; i++){
            chandelier[i] = new Bulb();
        }
    }

    // Включение ламп в люстре
    public void turnOn() {
        for (Bulb bulb:chandelier ) {
            bulb.turnOn();
        }
    }

    // Выключение ламп в люстре
    public void turnOff() {
        for (Bulb bulb:chandelier ) {
            bulb.turnOff();
        }
    }

    // Придумать бизнес - логику
    public boolean getToggle() {
        return this.chandelier[0].getToggle();
    }

}
