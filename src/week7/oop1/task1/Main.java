package week7.oop1.task1;

public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb(true);
        System.out.println("Светит ли сейчас лампа? - " + bulb.getToggle());
        bulb.turnOff();
        System.out.println("Светит ли сейчас лампа? - " + bulb.getToggle());
        Chandelier chandelier = new Chandelier(4);
        chandelier.turnOn();
        chandelier.turnOff();

    }
}
