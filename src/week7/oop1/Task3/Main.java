package week7.oop1.Task3;

public class Main {
    public static void main(String[] args) {
        System.out.println(FieldValidator.validateEmail("andy@bk.ru"));
        System.out.println(FieldValidator.validateDate("24.03.1970"));
        System.out.println(FieldValidator.validateNumber("+79115438765"));
        System.out.println(FieldValidator.validateName("Andy"));
    }
}
