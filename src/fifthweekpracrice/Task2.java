package fifthweekpracrice;

import java.util.Scanner;

/*
На вход подаётся число N - длина массива.
Затем передаётся массив целых чисел длины N.

Вывести элементы, стоящие на чётных индексах массива

4
20 20 11 13
-->
20 11
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);

        int n = scan.nextInt();

        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] =  scan.nextInt();
        }

//        for (int i = 0; i < arr.length; i++) {
//            if (i % 2 == 0) {
//                System.out.println(arr[i] + " ");
//            }
//        }
        for (int i = 0; i < arr.length; i+= 2) {
            System.out.println(arr[i] + " ");
        }

    }
}
