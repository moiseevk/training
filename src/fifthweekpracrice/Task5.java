package fifthweekpracrice;

import java.util.Scanner;

/*
 На вход подаётся число N - длина массива.
 Затем передаётся отсортированный по возрастанию массив целых различных чисел из N элементов.
 После этого число M.


 Найти в массиве все пары чисел, которые в сумме дают число M и вывести их на экран.
 Если таких нет, то вывести -1.
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);

        System.out.println("Введите длину массива: ");
        int n =  scan.nextInt();
        boolean flag = false;

        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] =  scan.nextInt();
        }
        System.out.println("Введите число, которая будет являться суммой некоторых чисел массива:");
        int m = scan.nextInt();


        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++ ) {
                if (arr[i] + arr[j] == m) {
                    System.out.println(arr[i] + " " + arr[j]);
                    flag = true;
                }
            }
        }
        if (!flag) {
            System.out.println(-1);
        }


    }
}
