package fifthweekpracrice;

import java.util.Scanner;

/*
На вход подаётся число N - длина массива.
 Затем передаётся массив строк длины N.
 После этого - число M.

 Сохранить в другом массиве только те элементы, длина строки которых не превышает M.

 Выходные данные:
 5
 Hello
 good
 to
 see
 you
 4

Выходные данные:
good to see you
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        String[] arr = new String[n];
        for (int i = 0; i< arr.length; i++) {
            arr[i] = scan.next();
        }

        int m = scan.nextInt();
      // getWords(m, arr);

        int k = 0;
        String[] result = new String[n];
        for (String s : arr) {
            if (s.length() <= m) {
                result[k++] = s;
            }
        }
        for (int i = 0; i < k; i++) {
            System.out.print(result[i] + " ");
        }

    }

//    public static String[] getWords(int n, String[] list) {
//        for (int i = 0; i < list.length; i++) {
//            if (list[i].length() <= n) {
//                System.out.print(list[i] + " ");
//            }
//        }
//        return list;
//    }
}
