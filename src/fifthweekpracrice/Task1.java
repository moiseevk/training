package fifthweekpracrice;

import java.util.Scanner;

/*
На вход подаётся число N - длина массива.
Затем передаётся массив целых чисел длины N.

Вывести всё чётные элементы массива
Если таких элементов нет, вывести -1.
 */
public class Task1 {
    public static void main(String[] args) {
        /*
        Алгоритм решения задачи:
        1) Объявляем переменную N со сканера - это длина массива
        2) Объявляется переменная массива и создаётся сам массив, длина которого равно числу N
        3) Выводим все чётные элементы массива
        4) Если таковых нет, то выводим -1
         */
        Scanner scan = new Scanner(System.in);

        System.out.print("Введите длину массива: ");
        int n = scan.nextInt();
        int[] massive = new int[n];
       //int uneven = 0;
        boolean flag = false;

        // Заполняем массив данными, количество которых не превышает значение переменной n
        for (int i = 0; i < massive.length; i++) {
            massive[i] =  scan.nextInt();
            }
        for (int i : massive) {
            if (i % 2 == 0){
                System.out.print(i + " ");
                flag = true;
            }
//            else if (i % 2 != 0) {
//                uneven++;
//                if (uneven == massive.length) {
//                    System.out.println(-1);
//                }
            }
        if (!flag) {
            System.out.println(-1);
        }
        }
    }
