package fifthweekpracrice;

import java.util.Scanner;
import java.util.Arrays;

/*
На вход подаются два отсортированных массива.
Нужно создать отсортированный третий массив, состоящий из элементов первых двух.
5
1 2 3 4 7

2
1 6

Выходные данные:
1 1 2 3 4 5 7

 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);

        int n = scan.nextInt();
        int[] arr1 = new int[n];
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] =  scan.nextInt();
        }

        int m = scan.nextInt();
        int[] arr2 = new int[m];
        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = scan.nextInt();
        }

        mergeTwoArraysWithLoop(arr1,arr2);
        mergeTwoArraysWithSystemArrayCopy(arr1,arr2);
        mergeTwoArrays(arr1, arr2);
    }

    /**
     * Метод делает слияние двух отсортированных массивов в третий результирующий
     * @param arr1 первый массив
     * @param arr2 второй массив
     */
    public static void mergeTwoArraysWithLoop(int[] arr1, int[] arr2) {
        int[] mergerArray = new int[arr1.length + arr2.length];

        int pos = 0;
        // Копируем элементы первого массива в результирующий
        for (int element : arr1) {
            mergerArray[pos] = element;
            pos++;
        }

        //Копируем элементы второго массива в результирующий
        for (int element : arr2) {
            mergerArray[pos] = element;
            pos++;
        }

        // Сортируем
        Arrays.sort(mergerArray);
        System.out.println(Arrays.toString(mergerArray));
    }

    /**
     * Метод объединяет два отсортированных массива в один результирующий
     * @param arr1 первый массив
     * @param arr2 второй массив
     */
    public static void mergeTwoArraysWithSystemArrayCopy(int[] arr1, int[] arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];
        System.arraycopy(arr1,0,mergedArray,0,arr1.length);
        System.arraycopy(arr2,0,mergedArray, arr1.length, arr2.length);
        Arrays.sort(mergedArray);
        System.out.println(Arrays.toString(mergedArray));
    }
    public static void mergeTwoArrays(int[] arr1, int[] arr2) {
        int[] mergeArray = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;

        //Обход двух массивов
        while(i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergeArray[k++] = arr1[i++];
            } else {
                mergeArray[k++] = arr2[j++];
            }
        }
            // mergedArray[k++] = arr1[i] < arr2[j] ? arr1[i++] : arr2[j++]

            //Сохраняем оставшиеся элементы первого массива
            while(i < arr1.length) {
                mergeArray[k++] = arr1[i++];
            }

            //Сохраняем оставшиеся элементы второго массива
            while(j < arr2.length) {
                mergeArray[k++] = arr2[j++];
            }
            System.out.println(Arrays.toString(mergeArray));

    }
}
