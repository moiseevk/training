package fifthweekpracrice;
import java.util.Scanner;
/*
На вход подаётся число N - длина массива.
Затем передаётся массив целых чисел длины N.

Проверить, является ли он отсортированным массивом по строго убыванию. Если да, то вывести true, иначе вывести false.
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = scan.nextInt();
        }
        System.out.println(chechIfArrayDesc(arr));
    }


    public static boolean chechIfArrayDesc(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if(array[i] <= array[i + 1]) {
                return false;
            }
        }
        return true;
    }
//        for (int i = 0; i < arr.length; i++) {
//            if (arr[i] > arr[i + 1]) {
//                if(arr[i] == arr.length - 1) {
//                    System.out.println(true);
//                    break;
//                }
//            }
//            else {
//                System.out.println(false);
//                break;
//            }
//        }



}
