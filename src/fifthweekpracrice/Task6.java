package fifthweekpracrice;

import java.util.Arrays;
import java.util.Scanner;

/*
 На вход подаётся число N - длина массива.
 Затем передаётся отсортированный по возрастанию массив целых различных чисел из N элементов.

 Нужно циклически сдвинуть элементы на 1 влево.

 Входные данные:
 5
 1 2 3 4 7
 Выходные данные:
 2 3 4 7 1
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] arr = new int[n];
        for (int i  = 0; i < arr.length; i++) {
            arr[i] = scan.nextInt();
        }

//        int temp = arr[0];
//        for (int i = 0; i < arr.length - 1; i++) {
//            arr[i] = arr[i + 1];
//        }
//        arr[arr.length - 1] = temp;
//        System.out.println(Arrays.toString(arr));


        // Такой вариант, чем выше
        int temp1 = arr[0];
        System.arraycopy(arr,1,arr,0,arr.length - 1);
        arr[arr.length - 1] = temp1;
        System.out.println(Arrays.toString(arr));
    }
}
