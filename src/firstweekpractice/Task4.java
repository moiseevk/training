package firstweekpractice;

/*

        Дана площадь круга, нужно найти диаметр окружности и длину окружности.
        S  = PI * (D^2 / 4) - это через диаметр => d = sqrt(S * 4 / PI)
        S = PI * r^2 - радиус
        S = L^2 / (4 *PI) - площадь через длину
        Отношение длины окружности к диаметру является постоянным числом.
        π = L : d

        Входные данные:
        91
        */
import java.util.Scanner;
public class Task4 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        double s = scan.nextDouble();
        final double PI = 3.14159;
        double di;
        double l;

        di = Math.sqrt(s * 4 / PI);
        System.out.println("Диаметр окружности: " + di);

        l = di * PI;
        System.out.println("Длина окружности: " + l);






    }
}
