package firstweekpractice;

/*
        Напишите аналог функции swap, которая меняет значения двух параметров местами (без вспомогательной переменной)
        Входные данные
        a = 8; b = 10
     */
import java.util.Scanner;
public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();

        // логика
        a = a + b; // 2 +5 = 7 -> a = 7
        b = a- b;  // 7 -5 = 2 -> b = 2
        a = a - b; // 7 -2 = 5 -> a = 5

        System.out.println("Результат");
        System.out.println("a=" + a + ";b=" + b);


    }
}
