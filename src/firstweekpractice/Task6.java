package firstweekpractice;

/*
        Дано двузначное число. Вывести сначала левую цифру (единицы), затем правую (десятки)
        */
import java.util.Scanner;
public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int c = scan.nextInt();

        System.out.println("Результат");
        System.out.println("Единицы: " + c % 10  + " Десятки: " + c / 10 );
    }
}
