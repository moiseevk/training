package sixthweekpractice;
/*
На вход подается натуральное число N.
Необходимо проверить, является ли оно степенью двойки (решить через рекурсию).
Вывести true, если является и false иначе.

4 -> true
5 -> false
6 -> false
7 -> false
8 -> true
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        boolean isTrueOrFalse = isTrueOrFalse(n);
        System.out.println(isTrueOrFalse);

    }

    public static boolean isTrueOrFalse(int number) {
        if (number == 2 || number == 1) {
            return true;
        }
        if (number <= 0 || number % 2 != 0) {
            return false;
        }
        else {
          return  isTrueOrFalse(number / 2);
        }
    }
}
