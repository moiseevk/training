package sixthweekpractice;
/*
Развернуть строку рекурсивно.

abcde -> edcba
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();

        String rev = reverse(s);
        System.out.println(rev);


    }
    public static String reverse(String s) {
        if (s.isEmpty()) {
            return s;
        }
        else {
            return reverse(s.substring(1)) + s.charAt(0);
        }

    }
}
