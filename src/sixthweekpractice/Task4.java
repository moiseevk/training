package sixthweekpractice;
/*
   Написать функцию через рекурсию для вычисления суммы заданных положительных целых чисел a b
   без прямого использования оператора +.
    */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        System.out.println(sum(a , b));


    }

    public static int sum(int a, int b) {
      if (b == 0) {
          return a;
      }
      else {
          return sum(a + 1, b - 1);
      }
    }
}
