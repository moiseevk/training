package sixthweekpractice;
/*
На вход передается N — высота двумерного массива и M — его ширина.
Затем передается сам массив.
Необходимо сохранить в одномерном массиве суммы чисел каждого столбца и вывести их на экран.
Пример:
Входные данные
2 2
10 20
5 7
Выходные данные
15 27
Входные данные
3 1
30
42
15
Выходные данные
87 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt(); // Высота массива
        int m = scan.nextInt(); // Ширина массива
        int[][] arr = new int[n][m];

        // Заполняем массив
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = scan.nextInt();
            }
        }
        sum(arr);
    }

        // Находим сумму чисел каждого столбца массива
    public static void sum(int[][] arr) {
        int[] res = new int[arr[0].length];
        for (int i = 0; i < arr[0].length; i++) {
            int total = 0;
            for (int j = 0; j < arr.length; j++) {
                res[i] += arr[j][i];
            }
            System.out.println(res[i]);
        }

    }
}
