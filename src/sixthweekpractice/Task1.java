package sixthweekpractice;

import java.util.Scanner;

/*
Найти факториал числа n рекурсивно
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        //System.out.println("Факториал числа " + n + " будет равен: " + fact(n));
        int res = factorialTail(n, 1);
        System.out.println(res);

    }

    public static int fact(int number) {
        if (number == 0) {
            return 1;
        } else {
            return number * fact(number - 1);
        }
    }

    public static int factorialTail(int n, int result) {
        if (n <= 1) {
            return result;
        } else {
            return factorialTail(n - 1, n * result);
        }


    }
}
